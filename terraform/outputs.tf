output "alb_arn" {
  description = "ALB ARN"
  value       = "${module.s3_bucket.s3_arn}"
}