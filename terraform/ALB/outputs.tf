output "alb_arn" {
  description = "The ALB ARN"
  value = "${aws_s3_bucket.s3_bucket_files.arn}"
}