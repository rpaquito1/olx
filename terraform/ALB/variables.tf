variable "deploy_name" {
  description = "The deployment name"
}

variable "aws_region" {
  description = "AWS Region"
}

variable "security_group" {
  description = "AWS securoty group"
}

variable "subnet" {
  description = "AWS subnet"
}